package View;

public interface ScannerMVC {

    interface Controller {

        int getOption();
        boolean setwrong(int value);

    }

    interface View {

    }
}
