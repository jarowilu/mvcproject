package View;

import Controller.CarSalonController;
import Controller.MainMenuController;
import Controller.ScannerController;

public class CarSalon implements CarSalonMVC.View, MainMenuMVC.View, ScannerMVC.View {

    private CarSalonMVC.Controller carSalonController;
    private MainMenuMVC.Controller menuController;
    private ScannerMVC.Controller scannerController;


    public CarSalon(){
        carSalonController = new CarSalonController();
        menuController = new MainMenuController();
        scannerController = new ScannerController();

        carSalonController.attach(this);
        menuController.attach(this);
        menuController.start();
    }

    public void showInitMsg() {

        menuController.showInitMsgOnConsole();
        int wallet = scannerController.getOption();
        carSalonController.initUser(wallet);
        menuController.goToAvaibleCars();

    }

    public void showAvaibleCars ()
    {
        menuController.printAvailableCar(carSalonController.getWallet());
        int opcion = scannerController.getOption();
        menuController.nextColor(carSalonController.setCarBrand(opcion));
    }
    public void showColor (){

        menuController.printAvaibleColors();
        int opcion = scannerController.getOption();
        menuController.nextFuel(carSalonController.setColor(opcion));

    }

    public void showFuel () {
        menuController.printAvaibleFuel();
        int opcion = scannerController.getOption();
        menuController.nextCarBody(carSalonController.setFuel(opcion));

    }

    public void showCarBody() {
        menuController.printAvaibleCarBody();
        int opcion = scannerController.getOption();
        menuController.carFinish(carSalonController.setBody(opcion));
    }

    public void showCar() {

        carSalonController.printCar();
    }

    public boolean wrongValue(int Value) {

        return scannerController.setwrong(Value);
    }

    public void errorMessage() {

        menuController.showErrorMessage();
    }
}
