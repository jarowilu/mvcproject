package View;

public interface MainMenuMVC {

    interface Controller {

        void attach(View view);
        void start ();
        void showInitMsgOnConsole();
        void nextColor (int value);
        void nextFuel (int value);
        void nextCarBody(int value);
        void printAvailableCar(int wallet);
        void printAvaibleColors();
        void printAvaibleFuel();
        void carFinish(int value);
        void showErrorMessage();
        void printAvaibleCarBody();
        void goToAvaibleCars();
    }

    interface View {
        void showInitMsg();
        void showAvaibleCars();
        void showColor ();
        boolean wrongValue(int Value);
        void errorMessage();
        void showFuel();
        void showCarBody();
        void showCar();
    }
}
