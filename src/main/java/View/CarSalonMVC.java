package View;

public interface CarSalonMVC {

    interface Controller {
         void attach(View view);
         void initUser (int wallet);
         int setCarBrand(int opcion);
         int setColor (int opcion);
         int setFuel (int opcion);
         int setBody (int opction);
         void printCar ();
         int getWallet();

    }
    interface View {
    }
}
