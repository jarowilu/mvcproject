package Controller;

import View.ScannerMVC;

import java.util.Scanner;

public class ScannerController implements ScannerMVC.Controller {

    private int wrongValue =0;

    public int getOption() {
        return  new Scanner(System.in).nextInt();
    }
    public boolean setwrong(int value) {

        if (wrongValue == 2 && value ==1 ){
            return false;
        }
        else {
            if (value == 0) {
                wrongValue = 0;
            } else {
                wrongValue += value;
            }

            return true;
        }
    }
}
