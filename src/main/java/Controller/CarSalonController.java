package Controller;

import Model.*;
import View.CarSalonMVC;

public class CarSalonController implements CarSalonMVC.Controller {

    private CarSalonMVC.View view;
    private Car car;
    private User user;


    public void attach(CarSalonMVC.View view) {
        this.view = view;
    }

    public void initUser(int wallet) {

        car =new Car();
        user = new User();
        user.walletChange(wallet);

    }

    public int setCarBrand(int opcion){

        switch (opcion){
            case 0: {
                car.setCarBrand(CarBrand.AUDI);
                if (carBrandChange()) {
                    return 1;
                } else {
                    return 2;
                }
            }
            case 1: {
                car.setCarBrand(CarBrand.MERCEDES);
                if (carBrandChange())
                {
                    return 1;
                }
                else
                    return 2;
            }
            case 2: {
                car.setCarBrand(CarBrand.FIAT);
                if (carBrandChange())
                {
                    return 1;
                }
                else
                    return 2;
            }
            case 3: {
                car.setCarBrand(CarBrand.KIA);
                if (carBrandChange())
                {
                    return 1;
                }
                else
                    return 2;

            }
            default:
            {
                return 2;
            }

        }


    }

    public int setColor (int opcion)
    {
        switch (opcion){
            case 0: {
                car.setColor(Color.WHITE);
                if (carColorChange()) {
                    return 1;
                } else {
                    return 2;
                }
            }
            case 1: {
                car.setColor(Color.SILVER);
                if (carColorChange())
                {
                    return 1;
                }
                else
                    return 2;
            }
            case 2: {
                car.setColor(Color.RED);
                if (carColorChange())
                {
                    return 1;
                }
                else
                    return 2;
            }
            case 3: {
                car.setColor(Color.BLUE);
                if (carColorChange())
                {
                    return 1;
                }
                else
                    return 2;
            }
            case 4:{
                car.setColor(Color.GREEN);
                if (carColorChange())
                {
                    return 1;
                }
                else
                    return 2;

            }
            case 5:{
                if (car.getColor() == null)
                {
                    user.walletChange(car.getCarBrand().getPrice());
                    car.setCarBrand(null);
                    return 3;
                }
                else {
                    user.walletChange(car.getColor().getCena());
                    user.walletChange(car.getCarBrand().getPrice());
                    car.setColor(null);
                    car.setCarBrand(null);
                }

            }
            default:
            {
                return 2;
            }
        }
    }
    public int setFuel (int option){
        switch (option){
            case 0: {
                car.setFuel(Fuel.BENZYNA);
                if (carFuelChange()) {
                    return 1;
                } else {
                    return 2;
                }
            }
            case 1: {
                car.setFuel(Fuel.DIESEL);
                if (carFuelChange())
                {
                    return 1;
                }
                else
                    return 2;
            }
            case 2: {
                car.setFuel(Fuel.HYBRYDA);
                if (carFuelChange())
                {
                    return 1;
                }
                else
                    return 2;
            }
            case 3:{
                if (car.getFuel() == null)
                {
                    user.walletChange(car.getColor().getCena());
                    car.setColor(null);
                    return 3;
                }
                else {
                    user.walletChange(car.getFuel().getPrice());
                    user.walletChange(car.getColor().getCena());
                    car.setColor(null);
                    car.setColor(null);
                    return 3;
                }
            }
            default:
            {
                return 2;
            }
        }
    }

    public int setBody (int opction){
        switch (opction){
            case 0: {
                car.setCarBody(CarBody.SEDAN);
                if (carBodyChange()) {
                    return 1;
                } else {
                    return 2;
                }
            }
            case 1: {
                car.setCarBody(CarBody.HATCHBACK);
                if (carBodyChange())
                {
                    return 1;
                }
                else
                    return 2;
            }
            case 2: {
                car.setCarBody(CarBody.COMBI);
                if (carBodyChange())
                {
                    return 1;
                }
                else
                    return 2;
            }
            case 3: {
                car.setCarBody(CarBody.PICK_UP);
                if (carBodyChange())
                {
                    return 1;
                }
                else
                    return 2;
            }

            case 4:{
                if (car.getCarBody() == null)
                {
                    user.walletChange(car.getFuel().getPrice());
                    car.setFuel(null);
                    return 3;
                }
                else {
                    user.walletChange(car.getCarBody().getPrice());
                    car.setCarBrand(null);
                    user.walletChange(car.getFuel().getPrice());
                    car.setFuel(null);
                    return 3;
                }
            }
            default:
            {
                return 2;
            }
        }
    }

    private boolean carBrandChange()
    {
        if (user.getWallet() > car.getCarBrand().getPrice())
        {
            user.walletChange(- car.getCarBrand().getPrice());
            return true;
        }
        else
        {
            car.setCarBrand(null);
            return false;
        }
    }
    private boolean carColorChange()
    {
        if (user.getWallet() > car.getColor().getCena())
        {
            user.walletChange(- car.getColor().getCena());
            return true;
        }
        else
        {
            car.setColor(null);
            return false;
        }
    }
    private boolean carFuelChange()
    {
        if (user.getWallet() > car.getFuel().getPrice())
        {
            user.walletChange(- car.getFuel().getPrice());
            return true;
        }
        else
        {
            car.setFuel(null);
            return false;
        }
    }
    private boolean carBodyChange()
    {
        if (user.getWallet() > car.getCarBody().getPrice())
        {
            user.walletChange(- car.getCarBody().getPrice());
            return true;
        }
        else
        {
            car.setFuel(null);
            return false;
        }
    }
    public void printCar() {
        System.out.println(car.getColor().toString() + "\t"+ car.getFuel().toString() + "\t"+ car.getCarBody().toString() + "\t"+ car.getCarBrand().toString() + "\t"+ user.getWallet());
    }

    @Override
    public int getWallet() {
        if (user != null)
        {
            return user.getWallet();
        }
        else
            return 0;
    }
}


