package Controller;

import Model.*;
import View.MainMenuMVC;

public class MainMenuController implements MainMenuMVC.Controller {

    private MainMenuMVC.View view;

    public void attach(MainMenuMVC.View view) {

        this.view = view;
    }

    public void showInitMsgOnConsole() {

        System.out.println("Witaj w salonie samochodowy");
        System.out.println("Podaj wartośc portfela");

    }

    public void start (){

        view.showInitMsg();
    }

    public void printAvailableCar(int wallet) {

        String avaibleCars ="";

        for (int i = 0; i< CarBrand.values().length ; i++)
        {
            if (CarBrand.values()[i].getPrice() <= wallet){

                 System.out.println(i + "\t" + CarBrand.values()[i].toString() + "\t" + CarBrand.values()[i].getPrice());
            }
        }

    }

    public void goToAvaibleCars(){
        view.showAvaibleCars();
    }

    public void printAvaibleColors() {
        for (int i =0 ; i < Color.values().length; i++)
        {
            System.out.println(i + "\t" + Color.values()[i].toString() + "\t" + Color.values()[i].getCena());
        }
        System.out.println("Cofnij");
    }

    public void printAvaibleFuel() {

        for (int i = 0; i < Fuel.values().length; i++)
        {
            System.out.println(i + "\t" + Fuel.values()[i].toString() + "\t" + Fuel.values()[i].getPrice());
        }
        System.out.println("Cofnij");
    }

    public void printAvaibleCarBody(){

        for (int i = 0; i < CarBody.values().length; i++)
        {
            System.out.println(i + "\t" + CarBody.values()[i].toString() + "\t" + CarBody.values()[i].getPrice());
        }
        System.out.println("Cofnij");
    }

    public void nextColor (int value)
    {
        if (value == 1){
            view.wrongValue(0);
            view.showColor();
        }
        else {
            if (view.wrongValue(1)) {
                view.showAvaibleCars();
            }
            else {
                view.errorMessage();
            }
        }
    }
    public void nextFuel (int value){

        if (value == 1){
            view.wrongValue(0);
            view.showFuel();
        }
        else if (value == 2)
        {
            if (view.wrongValue(1)) {
                view.showColor();
            }
            else {
                view.errorMessage();
            }
        }
        else  {
            view.showAvaibleCars();
        }


    }

    public void nextCarBody(int value) {

        if (value == 1){
            view.wrongValue(0);
            view.showCarBody();
        }
        else if (value ==2)
        {
            if (view.wrongValue(1)) {
                view.showFuel();
            }
            else {
                view.errorMessage();
            }
        }
        else {
            view.showColor();
        }
    }

    public void carFinish(int value) {

        if (value == 1){
            view.wrongValue(0);
            view.showCar();
        }
        else if (value ==2)
        {
            if (view.wrongValue(1)) {
                view.showCarBody();
            }
            else {
                view.errorMessage();
            }
        }
        else {
            view.showFuel();
        }
    }

    public void showErrorMessage() {

        System.out.println("ups cos poszło nie tak");
    }
}
