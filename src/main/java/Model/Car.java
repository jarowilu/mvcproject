package Model;


public class Car {

        CarBrand carBrand;
        Fuel fuel ;
        CarBody carBody;
        Color color;


        public CarBrand getCarBrand() {
                return carBrand;
        }

        public void setCarBrand(CarBrand carBrand) {
                this.carBrand = carBrand;
        }

        public Fuel getFuel() {
                return fuel;
        }

        public void setFuel(Fuel fuel) {
                this.fuel = fuel;
        }

        public CarBody getCarBody() {
                return carBody;
        }

        public void setCarBody(CarBody carBody) {
                this.carBody = carBody;
        }

        public Color getColor() {
                return color;
        }

        public void setColor(Color color) {
                this.color = color;
        }
}
