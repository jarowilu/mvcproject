package Model;

public enum CarBrand {

    AUDI(20000),
    MERCEDES(30000),
    FIAT(10000),
    KIA(15000);

    private int price;
    CarBrand(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }


}
